package models;

public class SubscriptIndex {
    boolean hasIndex;
    int index;

    public  SubscriptIndex() {
    }

    public SubscriptIndex(boolean hasIndex, int index) {
        this.hasIndex = hasIndex;
        this.index = index;
    }

    public boolean getHasIndex() {
        return hasIndex;
    }

    public void setHasIndex(boolean hasIndex) {
        this.hasIndex = hasIndex;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index=index;
    }

    public String toString() {
        if (index!=0)
        return "SubscriptIndex{" +
                "hasIndex=" + hasIndex +
                ", index=" + index +
                '}';
        else return "SubscriptIndex{" +
                "hasIndex=" + hasIndex +
                ", no index" +
                '}';
    }

}