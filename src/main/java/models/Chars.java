package models;

public class Chars {
    boolean color;
    int size;
    boolean glossy;
    SubscriptIndex subscriptIndex;

    public Chars() {
    }

    public Chars(boolean color, int size, boolean glossy, SubscriptIndex subscriptIndex) {
        this.color = color;
        this.size = size;
        this.glossy = glossy;
        this.subscriptIndex = subscriptIndex;
    }

    public boolean getColor() {
        return color;
    }

    public void setColor(boolean color) {
        this.color = color;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean getGlossy() {
        return glossy;
    }

    public void setGlossy(boolean glossy) {
        this.glossy = glossy;
    }

    public SubscriptIndex getSubscriptIndex() {
        return subscriptIndex;
    }

    public void setSubscriptIndex(SubscriptIndex subscriptIndex) {
        this.subscriptIndex = subscriptIndex;
    }

    public String toString() {
        return "Chars{" +
                "color=" + color +
                ", size=" + size +
                ", glossy=" + glossy +
                ", subscriptIndex=" + subscriptIndex +
                '}';
    }

}