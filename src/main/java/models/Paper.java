package models;

public class Paper {
    private int paperCode;
    private String name;
    private String type;
    private boolean monthly;
    private Chars chars;

    public Paper() {
    }

    public Paper(int paperCode, String name, String type, boolean monthly, Chars chars) {
        this.paperCode = paperCode;
        this.name = name;
        this.type = type;
        this.monthly = monthly;
        this.chars = chars;
    }

    public int getPaperCode() {
        return paperCode;
    }

    public void setPaperCode(int paperCode) {
        this.paperCode = paperCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean getMonthly() {
        return monthly;
    }

    public void setMonthly(boolean monthly) {
        this.monthly = monthly;
    }
    
    public Chars getChars() {
        return chars;
    }

    public void setChars(Chars chars) {
        this.chars = chars;
    }

    public String toString() {
        return "Paper{" +
                "paperCode=" + paperCode +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", monthly=" + monthly +
                ", chars=" + chars +
                '}';
    }
}