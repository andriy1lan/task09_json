package json;

import models.Paper;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.everit.json.schema.ValidationException;
import org.json.JSONException;
import java.util.Collections;
import java.util.Comparator;


public class JSONReader {
	
	  public static void main(String[] args) {
		  
		  File json = new File("src\\main\\resources\\papers.json");
		  File schema = new File("src\\main\\resources\\JSONSchema.json");
		  
		  try {
		  if (JSONSchemaValidator.validateJSON(schema.getName(), json.getName()))
		  System.out.println("Json file is validated against Json Schema");
		  }

		 catch(JSONException | IOException je) {System.out.println(je.getMessage()+ "::ERROR: Correct your json file");
		 return;}
		  
		  JSONParser parser = new JSONParser();
		    printList(parser.getPaperList(json));
		  }

		  private static void printList(List<Paper> papers) {
			Comparator comparator=Comparator.comparing(Paper::getName);
			Collections.sort(papers,comparator);
		    System.out.println("JSON: ");
		    for (Paper paper : papers) {
		      System.out.println(paper);
		    }
		  }

}
