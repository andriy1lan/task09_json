package json;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.Paper;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class JSONParser {
	
    private ObjectMapper objectMapper;

    public JSONParser() {
        this.objectMapper = new ObjectMapper();
    }
    
    public List<Paper> getPaperList(File jsonFile){
        Paper[] papers = new Paper[0];
        try{
            papers = objectMapper.readValue(jsonFile, Paper[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Arrays.asList(papers);
    }
	

}
