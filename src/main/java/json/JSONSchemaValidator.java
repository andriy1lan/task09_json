package json;

import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONTokener;
import java.io.IOException;
import java.io.InputStream;
import org.everit.json.schema.ValidationException;
import org.json.JSONException;

public class JSONSchemaValidator {
	
    public static boolean validateJSON(String jsonsxema, String jsonarray) throws JSONException,IOException {
    	
    	byte[] sxema = new byte[JSONSchemaValidator.class.getClassLoader().getResourceAsStream(jsonsxema).available()];
    	JSONSchemaValidator.class.getClassLoader().getResourceAsStream(jsonsxema).read(sxema);
    	//System.out.println("JSON Schema: " + new String(sxema).substring(new String(sxema).indexOf("{")));
    	
    	byte[] papers = new byte[JSONSchemaValidator.class.getClassLoader().getResourceAsStream(jsonarray).available()];
    	JSONSchemaValidator.class.getClassLoader().getResourceAsStream(jsonarray).read(papers);
    	//System.out.println("JSON Objects : " + new String(papers).substring(new String(papers).indexOf("[")));
    	
    	         JSONArray jsonSubject = new JSONArray(
    			 new JSONTokener(new String(papers).substring(new String(papers).indexOf("["))));
   		         //new JSONTokener(JSONSchemaValidator.class.getClassLoader().getResourceAsStream("papers.json")));
    	         JSONObject jsonSchema = new JSONObject(
    	         new JSONTokener(new String(sxema).substring(new String(sxema).indexOf("{"))));
    		    //  new JSONTokener(JSONSchemaValidator.class.getClassLoader().getResourceAsStream("JSONSchema.json")));		    
    		    Schema scheme = SchemaLoader.load(jsonSchema);
    		    scheme.validate(jsonSubject);
                return true;
    }

}
